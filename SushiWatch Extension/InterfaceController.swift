//
//  InterfaceController.swift
//  SushiWatch Extension
//
//  Created by Prabhjinder Singh on 2019-10-30.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    // score variable
    var score = 0
    
    @IBOutlet weak var gameTimeLabel: WKInterfaceLabel!
    
    @IBOutlet weak var powerUpButton: WKInterfaceButton!
    var catSide = ""
    
    @IBOutlet weak var pauseResumeButton: WKInterfaceButton!
    
    @IBOutlet weak var saveScoreButton: WKInterfaceButton!
    
    var gamePaused = false
    var gameOver = false
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("time message recieved from phone")
        
        var timeLeft = message["timeLeft"] as! String
        var powerUpVisibilty = message["powerUp"] as! String
        var inScore = message["score"] as! String
        self.score = Int(inScore)!
        print("score: \(self.score)")
        
        var timeOverDone = Int(timeLeft)
        
        if(timeLeft == "15" || timeLeft == "10" || timeLeft == "5"){
        self.gameTimeLabel.setText("Time Left: \(timeLeft) seconds")
        }
            
        
       else if(timeOverDone! <= 0){
            self.gameTimeLabel.setText("Game Over!")
            self.saveScoreButton.setHidden(false)
            self.gameOver = true

        }
        else{
            self.gameTimeLabel.setText("")
        }

        // set visibilty of the powerup button true
        if(powerUpVisibilty == "visible"){
            self.powerUpButton.setHidden(false)

        }
        else if(powerUpVisibilty == "hidden"){
            self.powerUpButton.setHidden(true)
        }
        
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
         //self.gameTimeLabel.setText("")
        self.powerUpButton.setHidden(true)
        self.saveScoreButton.setHidden(true)
        
        if(WCSession.isSupported() == true){
            print("watch support WC session")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            print("watch do not support wc session")
            
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    // move the cat on left side

    @IBAction func leftButtonPressed() {
        if(self.gamePaused == false && self.gameOver == false){
        print("watch left button pressed")
        if(WCSession.default.isReachable == true){
            let message =  ["button": "left","powerUp": "", "saveScore": "false", "playerName": "", "gamePaused": "\(self.gamePaused)" ]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("message sent to phone from watch")
            self.catSide = "left"
        }
        else{
            print("message sent from watch")
        }
        }
    }
    // move the cat on right side
    @IBAction func rightButtonPressed() {
        
        if(self.gamePaused == false && self.gameOver == false){
        print("watch right button pressed")
        if(WCSession.default.isReachable == true){
            let message =  ["button": "right","powerUp": "", "saveScore": "false", "playerName": "", "gamePaused": "\(self.gamePaused)" ]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("message sent to phone from watch")
            self.catSide = "right"
        }
        else{
            print("message sent from watch")
        }
        }
    }
    
    
    // power up button to increase player power
    
    @IBAction func powerUpButtonPressed() {
        
        print("watch power up button pressed")
        if(WCSession.default.isReachable == true){
            let message =  ["button": "", "powerUp": "on", "saveScore": "false", "playerName": "", "gamePaused": "\(self.gamePaused)" ]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("message sent to phone from watch")
        }
        else{
            print("message sent from watch")
        }
    }
    
    // save the player name and score to cloud
    @IBAction func saveButtonPressed() {
        
        let suggestedResponses = ["Abc", "Def"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                let inputName1 = userResponse!
                if(WCSession.default.isReachable == true){
                    // save max three letters in name
                    let message =  ["button": "", "powerUp": "on", "saveScore": "true", "playerName": "\(inputName1.prefix(3))", "gamePaused": "\(self.gamePaused)" ]
                    WCSession.default.sendMessage(message, replyHandler: nil)
                    print("message sent to phone from watch")
                }
                else{
                    print("message sent from watch")
                }
               
            }
        }
    }
    
    // pause the game
    @IBAction func pauseResumeButtonPressed() {
        if(self.gamePaused == false){
            self.gamePaused = true
            self.pauseResumeButton.setTitle("Resume")
            self.powerUpButton.setHidden(true)
            if(WCSession.default.isReachable == true){
                let message =  ["button": "", "powerUp": "", "saveScore": "false", "playerName": "", "gamePaused": "\(self.gamePaused)" ]
                WCSession.default.sendMessage(message, replyHandler: nil)
                print("message sent to phone from watch")
            }
            else{
                print("message sent from watch")
            }
        }
        else {
            self.gamePaused = false
            self.pauseResumeButton.setTitle("Pause")
            if(WCSession.default.isReachable == true){
                let message =  ["button": "", "powerUp": "", "saveScore": "false", "playerName": "", "gamePaused": "\(self.gamePaused)" ]
                WCSession.default.sendMessage(message, replyHandler: nil)
                print("message sent to phone from watch")
            }
            else{
                print("message sent from watch")
            }
        }
    }
    
}
