//
//  GameScene.swift
//  Assignment1
//
//  Created by Prabhjinder Singh on 2019-10-30.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//
import SpriteKit
import GameplayKit
import WatchConnectivity
import FirebaseDatabase

class GameScene: SKScene, WCSessionDelegate {
   
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    //high score button
    let highScoreButton = SKSpriteNode(imageNamed:"highScoreButton")
    // Make a tower
    var sushiTower:[SKSpriteNode] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    
    // Make chopsticks
    var chopstickGraphicsArray:[SKSpriteNode] = []
    
    // Make variables to store current position
    var catPosition = "left"
    var chopstickPositions:[String] = []
    
    
    //Timer
    let timer = SKSpriteNode(imageNamed: "life")
    let timer1 = SKSpriteNode(imageNamed: "life_bg")
    
    var counter = 0
    var counterTimer = Timer()
    var counterStartValue = 25
    var timerWidthPerSec = CGFloat(0.0)
    
    var random1 = 0
    var random2 = 0
    var randomNumberCounter = 1
    var firstPowerUp = 0
    var secondPowerUp = 0
    
    // score
    var score = 0
    
    var saveScore = false
    var playerName = ""
    
    // game paused or not
    var gamePaused = false
    
   // firebase
    var databaseReference: DatabaseReference?
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("message recieved from watch")
       // var buttonPressed = "left"
        var buttonPressed = message["button"] as! String
        var powerUp = message["powerUp"] as! String
        
        var saveScore = message["saveScore"] as! String
        self.saveScore = Bool(saveScore)!
        self.playerName = message["playerName"] as! String
        
        var gamePaused = message["gamePaused"] as! String
        self.gamePaused = Bool(gamePaused)!
        
        
        print("test : \(self.saveScore) ...... \(self.playerName)")
        print("test game paused : \(self.gamePaused)")
        
        print("button Pressed: \(buttonPressed)")
        
        self.moveCat(button: buttonPressed)
        
        if(powerUp == "on"){
            print(self.counter)
            print("power up pressed")
            if(timerWidthPerSec*25 <= self.timer.size.width+self.timerWidthPerSec*10){
                self.counter = 25
                self.timer.size = CGSize(width:self.timerWidthPerSec*25, height:self.timer.size.height)
            }
            else{
            self.counter = self.counter + 10
            self.timer.size = CGSize(width:self.timer.size.width+self.timerWidthPerSec*10, height:self.timer.size.height)
            print(self.counter)
            }
        }
    }
    
    
  
    
    func startCounter(){
       
        // create 2 random numbers
        self.random1 = Int.random(in: 1...25)
        self.random2 = Int.random(in: 1...25)
        
        // check which random number is greater and set it as first power up to appear
        if(self.random1 >= self .random2){
            self.firstPowerUp = random1
            self.secondPowerUp = random2
        }
        else{
            self.firstPowerUp = random2
            self.secondPowerUp = random1
        }
        
        print("first power up: \(self.firstPowerUp)")
        print("second power up: \(self.secondPowerUp)")
        
        // set width of the timer for 1 second
        timerWidthPerSec = timer.size.width/25
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(decrementCounter), userInfo: nil, repeats: true)
        
       
    }
    @objc func decrementCounter(){
        
        if(self.gamePaused == false){
            
            // decrementing the counter
            counter = counter - 1
        
        if(self.saveScore == true){
            // save scores and names to firebase
           // self.databaseReference?.child("Scores").childByAutoId().setValue(["Name: \(self.playerName), Score: \(self.score)"])
            
            
            let dataToUpload : [String : String] = ["Player Name: " : self.playerName, "Score: " : "\(self.score)"]
            self.databaseReference?.child("Scores").childByAutoId().setValue(dataToUpload)
            self.saveScore = false
        }
        
        
        
        if(Int(self.timer.size.width-timerWidthPerSec) >= 0){
            
            // decrease the width od the counter with time
            timer.size = CGSize(width:self.timer.size.width-timerWidthPerSec, height:self.timer.size.height)
            print("timer size.....\(self.timer.size)")
            print("score.....\(self.score)")
            // increasing the score
            self.score = self.score + 1
            
        }
        
        
            if(WCSession.default.isReachable == true){
              //  let message =  ["timeLeft": "\(counter)"]
                //WCSession.default.sendMessage(message, replyHandler: nil)
               
                if(counter == self.firstPowerUp && self.randomNumberCounter == 1){
                    
                    
                    let message1 =  ["timeLeft": "\(counter)", "powerUp": "visible", "score": "\(self.score)"]
                    WCSession.default.sendMessage(message1, replyHandler: nil)
                    self.randomNumberCounter = self.randomNumberCounter + 1;
                    
                    print("visible")
                
                }
                else  if(counter == self.secondPowerUp && self.randomNumberCounter == 2){
                    
                    
                    let message1 =  ["timeLeft": "\(counter)", "powerUp": "visible", "score": "\(self.score)"]
                    WCSession.default.sendMessage(message1, replyHandler: nil)
                    self.randomNumberCounter = self.randomNumberCounter + 1;
                    
                    print("visible")
                    
                }
                    
                    
                    
//               else if(counter == random1+2 || counter == random2+2){
//                    let message1 =  ["timeLeft": "\(counter)", "powerUp": "hidden"]
//                    WCSession.default.sendMessage(message1, replyHandler: nil)
//                    print("hidden")
//                }
//
                
                else{
                    let message1 =  ["timeLeft": "\(counter)", "powerUp": "hidden", "score": "\(self.score)"]
                    WCSession.default.sendMessage(message1, replyHandler: nil)
                    print("hidden")
                }
                print("message sent to phone from watch")
            }
            else{
                print("message sent from watch")
            }
        }
    }
    override func didMove(to view: SKView) {
        
        //firebase
        databaseReference = Database.database().reference()
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        //timer
        // add cat
        timer.anchorPoint = CGPoint(x: 0.0,y: 0.0)
        timer.position = CGPoint(x:self.size.width*0.07, y:self.size.height-112)
        
        timer.zPosition = 2
        addChild(timer)
        timer1.position = CGPoint(x:self.size.width*0.25, y:self.size.height-100)
        addChild(timer1)
        
        // high score button
        
        self.highScoreButton.position = CGPoint(x:self.size.width*0.90, y:self.size.height-100)
        addChild(self.highScoreButton)
        
        counter = counterStartValue
        startCounter()
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        if(WCSession.isSupported() == true){
            
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("phone supported the WC session")
        }
        else{
            print("wc session not supported in phone")
        }
    }
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SKSpriteNode(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
        
   
        
        // generate a number between 1 and 2
        let stickPosition = Int.random(in: 1...3)
        print("Random number: \(stickPosition)")
        if (stickPosition == 1) {
            // save the current position of the chopstick
            self.chopstickPositions.append("right")
            
            // draw the chopstick on the screen
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x + 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick object to the array
            self.chopstickGraphicsArray.append(stick)
            
            // redraw stick facing other direciton
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            stick.run(facingRight)
        }
        else if (stickPosition == 2) {
            // save the current position of the chopstick
            self.chopstickPositions.append("left")
            
            // left
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x - 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick to the array
            self.chopstickGraphicsArray.append(stick)
        }
        else{
            // save the current position of the chopstick
            self.chopstickPositions.append("")
            
            // left
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x
            stick.zPosition = -1
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick to the array
            self.chopstickGraphicsArray.append(stick)
        }
        
        
        // Add this if you cannot see the chopsticks
        // sushi.zPosition = -1
        
       
        
        
    }
    
    
  

    
    func buildTower() {
        for _ in 0...5 {
            self.spawnSushi()
        }
        for i in 0...5 {
            print(self.chopstickPositions[i])
        }
        
    }
 
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    func  moveCat(button : String){
        
        // ------------------------------------
        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
        //  When person taps mouse,
        //  remove a piece from the tower & redraw the tower
        // -------------------------------------
        let pieceToRemove = self.sushiTower.first
        let stickToRemove = self.chopstickGraphicsArray.first
        
        if (pieceToRemove != nil && stickToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // STICK: hide it from screen & remove from game logic
            stickToRemove!.removeFromParent()
            self.chopstickGraphicsArray.remove(at:0)
            
            // STICK: Update stick positions array:
            self.chopstickPositions.remove(at:0)
            
            // add a piece
            self.spawnSushi()
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // STICK: loop through the remaining sticks and redraw
            for stick in chopstickGraphicsArray {
                stick.position.y = stick.position.y - SUSHI_PIECE_GAP
            }
        }
        
        // ------------------------------------
        // MARK: SWAP THE LEFT & RIGHT POSITION OF THE CAT
        //  If person taps left side, then move cat left
        //  If person taps right side, move cat right
        // -------------------------------------
        
        // 1. detect where person clicked
        
      //  let middleOfScreen  = self.size.width / 2
        if (button == "left") {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
            
            // save cat's position
            self.catPosition = "left"
            
        }
        else {
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            cat.position = CGPoint(x:self.size.width*0.85, y:100)
            
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
            
            // save cat's position
            self.catPosition = "right"
        }
        
        // ------------------------------------
        // MARK: ANIMATION OF PUNCHING CAT
        // -------------------------------------
        
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        
        
        // ------------------------------------
        // MARK: WIN AND LOSE CONDITIONS
        // -------------------------------------
        
        // 1. if CAT and STICK are on same side - OKAY, keep going
        // 2. if CAT and STICK are on opposite sides -- YOU LOSE
        
        
        let firstChopstick = self.chopstickPositions[0]
        if (catPosition == "left" && firstChopstick == "left") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "right") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "left" && firstChopstick == "right") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "left") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        

        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        
//        let touch = touches.first!
//        let location = touch.location(in: self)
       
        // press the high score button to get the scores
        if self.highScoreButton.contains(mousePosition){
            print("get the high score")
            
         // retrieving data from the firebase
            let databaseRef = Database.database().reference(withPath: "Scores")
            databaseRef.observe(.value) { (snapshot) in
                snapshot.children.forEach({ (child) in
                    if let child = child as? DataSnapshot, let value = child.value {
                        print("Scores : \(value)")    //"Sean", "Yuh"
                        // here you can check for your desired user

                    }
                })
            }

       }
        

        print(mousePosition)
        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
           // self.moveCat(button: "left")
            
        }
        else{
           // self.moveCat(button: "right")
        }
      
        
    }
 
}
